<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/equipe', function () {
    return view('equipe');
});
Route::get('/histoire', function () {
    return view('histoire');
});
Route::get('/mission', function () {
    return view('mission');
});
Route::get('/nous', function () {
    return view('nous');
});
Route::get('/pageservices', function () {
    return view('pageservices');
});Route::get('/partenaires', function () {
    return view('partenaires');
});
Route::get('/projet', function () {
    return view('projet');
});
Route::get('/propos', function () {
    return view('propos');
});
Route::get('/partenaire', function () {
    return view('partenaire');
});
Route::get('/welcome', function () {
    return view('welcome');
});
Route::get('/projet1', function () {
    return view('projet1');
});
Route::get('/projet2', function () {
    return view('projet2');
});
Route::get('/projet3', function () {
    return view('projet3');
});
Route::get('/projet4', function () {
    return view('projet4');
});
Route::get('/projet1gal', function () {
    return view('projet1gal');
});
Route::get('/projet2gal', function () {
    return view('projet2gal');
});
Route::get('/projet3gal', function () {
    return view('projet3gal');
});
Route::get('/projet4gal', function () {
    return view('projet4gal');
});
Route::get('/don', function () {
    return view('don');
});
Route::get('/adminindex', function () {
    return view('adminindex');
});
Route::get('/adminindex2', function () {
    return view('adminindex2');
});
Route::get('/buttons', function () {
    return view('buttons');
});
Route::get('/calendar', function () {
    return view('calendar');
});
Route::get('/charts', function () {
    return view('charts');
});
Route::get('/form-common', function () {
    return view('form-common');
});
Route::get('/form-validation', function () {
    return view('form-validation');
});
Route::get('/form-wizard', function () {
    return view('form-wizard');
});
Route::get('/gallery', function () {
    return view('gallery');
});
Route::get('/grid', function () {
    return view('grid');
});
Route::get('/interface', function () {
    return view('interface');
});
Route::get('/invoice', function () {
    return view('invoice');
});
Route::get('/login', function () {
    return view('login');
});
Route::get('/problem', function () {
    return view('problem');
});
Route::get('/tables', function () {
    return view('tables');
});
Route::get('/widgets', function () {
    return view('widgets');
});
Route::get('/register', function () {
    return view('register');
});
