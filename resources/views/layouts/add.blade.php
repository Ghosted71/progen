<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ProGen Bénin</title>
    <link rel="shortcut icon" href="../img/logos/hand2.JPG"/>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <style>
        /* Make the image fully responsive */
        .carousel-inner img {
            width: 100%;
            height: 100%;
        }
        .carousel-inner >  {
            position: relative;
            display: none;
            -webkit-transition: 0.6s ease-in-out left;
            -moz-transition: 0.6s ease-in-out left;
            -o-transition: 0.6s ease-in-out left;
            transition: 0.6s ease-in-out left;
        }
    </style>

    <!-- Custom styles for this template -->
    <link href="css/agency.css" rel="stylesheet">
    <link href="css/style1.css" rel="stylesheet">
    


</head>

<body id="page-top" style="background-color: #E5E1D6">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand js-scroll-trigger" style="color: black;" href="/">
        <img src="../img/logos/hand2.JPG" style="width: 50px;hight:500px;"><span style="color:#E5E1D6">.....</span>
     <strong style="font-size:35px;">ProGen Bénin</strong></a>
    <div class="container" style="background-color: steelblue;" >
       
        
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" style="color: #F5F5DC;" href="propos"><strong>A propos</strong></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" style="color: #F5F5DC;" href="equipe"><strong>Equipe</strong></a>
                </li>
                 <li class="nav-item">
                   <a class="nav-link js-scroll-trigger" style="color: #F5F5DC;" href="nous"><strong>Projets</strong></a>
                 </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" style="color: #F5F5DC;" href="pageservices"><strong>Gallerie</strong></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" style="color: #F5F5DC;" href="don"><strong>Don</strong></a>
                </li>
                <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" style="color: #F5F5DC;" href="contact"><strong>Contact</strong></a>
                    </li>

            </ul>
            
        </div>
    </div>
</nav>
<div style="border-left: 60px solid steelblue;border-right: 60px solid steelblue;">
@yield('content')
</div>
<!-- Footer -->
<footer class=footer fixed-bottom>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <span class="copyright" style="color:steelblue;">Copyright 2018 &copy; ProGen Bénin</span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <li class="list-inline-item">
                        <a href="mailto:admin@progenbenin.org?Subject=Salut%ProGen" target="_top"  >
                            <i class="fa fa-envelope-o"></i>
                        </a>
                    </li>
                    <li class="list-inline-item" >
                        <a href="https://web.facebook.com/prochainegenbenin/">
                            <i class="fa fa-facebook" ></i>
                        </a>
                    </li>
                    <li class="list-inline-item" >
                        <a href="https://www.instagram.com/progenbenin/?hl=en">
                            <i class="fa fa-instagram" ></i>
                        </a>
                    </li>
                    
                    <li class="list-inline-item">
                        <a href="tel:00922966880691" >
                            <i class="fa fa-phone"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <li class="list-inline-item">
                        <a href="contact" style="color:steelblue;">Rejoins-nous</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="don" style="color:steelblue;">Faire un don</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Contact form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="js/agency.min.js"></script>

</body>

</html>
