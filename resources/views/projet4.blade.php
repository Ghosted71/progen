@extends('layouts.add')
@section('content')
<br/>
<div class="page-header">
        <div class="container">
            <div class="row"><br/>
                <div class="col-12">
                
                    <h1>Enfance Epanouie</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="highlighted-cause">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7 order-2 order-lg-1">
                    <div class="entry-content mt-5" style="text-align:justify">
                        <p>"Enfance Epanouie" est un projet qui permet aux enfants de développer leurs compétences sociales, intellectuelles et linguistiques aux travers des jeux. C’est ainsi que les enfants découvrent et comprennent le monde qui les entoure ainsi que la notion du plaisir et d’échange ce qui est indispensable à leur épanouissement.
                            C’est dans cette optique que le projet met à la disposition des écoles maternelles publiques des milieux ruraux et de leurs élèves des jouets par le biais de dons de jouets ou financiers, permettant aux enfants de grandir avec toute la confiance nécessaire.
                            <br/>
                            L'édition 2018 de ce projet a consisté en un don d'aire de jeux aux enfants de la maternelle de l'école primaire publique de Savi Djègo dans la commune de Ouidah. </p>
                    </div><!-- .entry-content -->

                    

                    <div class="entry-footer mt-6">
                        <a href="don" class="btn btn-primary" style="color:white">Faire un don</a>
                        <a href="projet4gal" class="btn btn-primary" style="color:white">Photos</a>
                    </div><!-- .entry-footer -->
                </div><!-- .col -->

                <div class="col-12 col-lg-5 order-1 order-lg-2">
                    <img src="img/gal/34.JPG" alt="" style="width:312px;height:289px">
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .highlighted-cause -->



   
    @endsection