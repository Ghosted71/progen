@extends('layouts.add')
@section('content')
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-12">
                  
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="contact-page-wrap" style="background-image:url('../img/bigsmile.jpg');">
    
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-5">
                    <div class="entry-content">
                        <h2>Contactez nous</h2>
                        <ul class="contact-info p-0">
                            <li><i class="fa fa-phone"></i><span style="color:black">+229 94 44 03 03 / <br/> +229 69 74 00 00</span></li>
                            <li><i class="fa fa-envelope"></i><span style="color:black">admin@progenbenin.org</span></li>
                        </ul>
                    </div>
                </div><!-- .col -->

                <div class="col-12 col-lg-7">
                    <form class="contact-form">
                        <input type="text" placeholder="Nom">
                        <input type="email" placeholder="Email">
                        <textarea rows="15" cols="6" placeholder="Votre message"></textarea>

                        <span>  <a href="mailto:ghislaine.akakpo@progenbenin.org?Subject=Salut%ProGen" target="_top">
                            <input class="btn btn-primary" type="submit" value="Contactez nous">
                        </a>
                        </span>
                    </form><!-- .contact-form -->

                </div><!-- .col -->


            </div><!-- .row -->
        </div><!-- .container -->
    </div>
    @endsection