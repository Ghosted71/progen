@extends('layouts.add')
@section('content')
<section  id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Nos projets</h2>
            <h3 class="section-subheading text-muted">Vivez notre vecu.</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6 portfolio-item">
            <a class="portfolio-link"  href="projet1">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/1.JPG" alt="Etude" style="width:400px;height:300px">
            </a>
            <div class="portfolio-caption">
              <h4>Objectif zéro grossesse </h4>
              <p class="text-muted"></p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 portfolio-item">
            <a class="portfolio-link"  href="projet2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/10.JPG" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Give a smile</h4>
              <p class="text-muted"></p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 portfolio-item">
            <a class="portfolio-link"  href="projet3">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/3.jpeg" alt=""style="width:400px;height:300px">
            </a>
            <div class="portfolio-caption">
              <h4>Excellence Féminine </h4>
              <p class="text-muted"></p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 portfolio-item">
            <a class="portfolio-link"  href="projet4">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/25.JPG" alt="" style="width:400px;height:300px">
            </a>
            <div class="portfolio-caption">
              <h4>Enfance épanouie</h4>
              <p class="text-muted"></p>
            </div>
          </div>
         
         
        </div>
      </div>
    </section>

@endsection