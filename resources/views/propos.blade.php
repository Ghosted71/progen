@extends('layouts.add')
@section('content')
<div class="page-header">
  <div class="container">
      <div class="row">
          <div class="col-12">
              <h1>A Propos</h1>
          </div><!-- .col -->
      </div><!-- .row -->
  </div><!-- .container -->
</div><!-- .page-header -->

<div class="welcome-wrap">

  <div class="container">
      <div class="row">
          <div class="col-12 col-lg-6 order-2 order-lg-1">
              <div class="welcome-content">
                  <header class="entry-header">
                      <h2 class="entry-title"></h2>
                  </header><!-- .entry-header -->

                  <div class="entry-content mt-5">
                      <div class="col-12">
                          <h1 class="entry-content" style="color:steelblue;font-style:unset;font-size:28px;"><strong>Présentation</strong></h1>
                      </div>
 
                      <p style="text-align: justify;color:black;font-style:initial;">

                      Prochaine Génération Bénin (ProGen Bénin), est une organisation non gouvernementale à but non lucratif enregistrée sous le n°2017|1909|DEP-LIT/SG/SAG-Assoc du 12 Avril 2017 et publié au Journal Officiel du 15 Juin 2017.
                      Intervenant dans le domaine de l’éducation et œuvrant à en faire un droit pour tous dans des conditions adéquates et dans un environnement positif, nos groupes cibles sont essentiellement les enfants, les femmes et les associations de parents d’élèves.
                      Nos projets, intervenant sur l’ensemble du territoire national, plus particulièrement dans le Littoral, l’Atlantique, le Borgou et l’Alibori, visent:<br/><br/>
                      <strong style="color:steelblue">• L’éducation formelle des enfants et adolescents</strong> <br/>
                      <strong style="color:steelblue">• La santé sexuelle et de la reproduction en milieu scolaire</strong><br/>
                      <strong style="color:steelblue">• La formation professionnelle des filles déscolarisées</strong><br/>
                      <strong style="color:steelblue">• L’appui au renforcement des capacités des femmes</strong><br/>
                      </p>
                  </div>

                  <br/><!-- .entry-footer -->
              </div><!-- .welcome-content -->
              <img src="../img/gal/26.JPG" alt="welcome" style="width:500px;height:700px">
          </div><!-- .col -->

          <div class="col-12 col-lg-6 order-1 order-lg-2">
              <img src="../img/gal/10.JPG" alt="welcome" style="width:600px;height:400px">
              <div class="container">
                  <div class="row">

                          <div class="welcome-content">


                              <div class="entry-content mt-5">

                                      <div class="col-12 col-lg-12 order-2 order-lg-1">
                                          <h1 class="entry-content" style="color:steelblue;font-style:unset;font-size:28px;"><strong>Mission et Valeurs</strong></h1><br/>
                                          <p style="text-align: justify;color:black;font-style:initial;">
                                              Tout en œuvrant pour la protection des droits des enfants et des femmes, ProGen Bénin a pour mission de procurer aux enfants démunis du Bénin une éducation de qualité et forfaitaire dans un cadre positif en leur donnant plein accès à l’information.
                                              Consciente que l'éducation est la base de tout développement, nous nous sommes s'est fixé de nombreux objectifs dans ce domaine. Il s'agit entre autres de : <br/><br/>
                                              •  Contribuer à l'amélioration des conditions de vie des élèves sur toute l'étendue du territoire <br/>
                                              • Mobiliser des ressources aux communautés et leurs écoles dans le but d'améliorer la qualité de l'éducation<br/>
                                              • Promouvoir un plein équitable accès des enfants aux centres d'enseignement de base dans un environnement sain et positif<br/> 

                                              ProGen, à travers ses activités, met en évidence les valeurs appliquées afin d’atteindre nos objectifs.<br/><br/>
                                              <strong style="color:steelblue">Dévouement et Egalité :</strong> Motivés sur un niveau personnel ainsi que professionnel, notre équipe se dévoue entièrement à aider ceux en difficultés. Nous avons foi que l’aide que nous apportons peut changer le cours d’une vie et donner une opportunité à ceux qui en ont besoin,sans distinction de leur age,sexe,orientation politique ou religieuse. Nous croyons au droit à l'education pour tous, et oeuvrons pour creer une reelle difference dans la vie de ceux que nous aidons.<br/><br/>
                                              <strong style="color:steelblue">Service et Excellence :</strong> Nous appuyons notre motivation par des actes, des plans et des projets afin de maximiser l’efficacité de l’aide que nous apportons. Nous nous assurons de toujours viser l'exellence et l’efficacité de notre travail afin d'assurer un maximum d'impact et de reussite dans nos projets. Pour ce faire, nous faisons appels à toutes méthodes disponibles et  travaillons avec des partenaires, quel que soit leur domaine, dans le but de servir la prochaine génération.<br/><br/>
                                              <strong style="color:steelblue">Intégrité et Transparence :</strong> En toutes choses, nous respectons et suivons nos valeurs et notre mission. Toutes nos intentions sont axés sur fournir une aide aux femmes et aux enfants en difficultés, et luttons contre toutes pratiques néfastes et illicites qui peuvent y nuire. Croyant la transparence absolue, nous sommes honnêtes envers nos partenaires, nos volontaires et contributeurs concernant notre motivation, nos pratiques, notre gestion, l’allocation des fonds et le résultat de nos projets.<br/><br/>

                                          </p>
                                      </div>
                                  </div>
                              </div>
          </div><!-- .col -->
      </div><!-- .row -->
  </div><!-- .container -->
</div><!-- .home-page-icon-boxes -->


</div>
</div>
    @endsection