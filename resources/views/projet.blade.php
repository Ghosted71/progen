@extends('layouts.add')
@section('content')
<section class="bg" id="portfolio">
    <div id="page1">
<div class="container" style="background-color:#9ACD32;border:2px #9ACD32;border-radius:15px 50px;" >
<div class="container">
        <div class="intro-text">
            <div class="container ">
                <strong> <em> Objectif zéro grossesse</em> </strong> :</br>

                « Objectif zéro grossesse  » est un projet qui consiste en des campagnes de sensibilisation. Ces campagnes sont organisées au profit des jeunes filles adolescentes en milieu scolaire. L’objectif est de les informer sur les causes, risques et conséquences des grossesses en milieu scolaire ainsi que les méthodes de prévention.
                2732 cas de grossesses ont été enregistrés au cours de l’année scolaire 2016-2017 et c’est pour remédier à cet état de choses qui joue sur l’éducation des filles que nous allons à leur rencontre.
                Ce projet est prévu pour s’étendre aux départements les plus touchés selon la classification du Ministère de l’Enseignement.
    
</br>
</br>
            </div>

        </div>
      </div>
</div>
    </div>
</br>
</br>
<div id="page2">
<div class="container" style="background-color:#9ACD32;border:2px #9ACD32;border-radius:15px 50px;" >
<div class="container">
        <div class="intro-text">
            <div class="container ">
                <strong> <em> Objectif zéro grossesse</em> </strong> :</br>

                « Objectif zéro grossesse  » est un projet qui consiste en des campagnes de sensibilisation. Ces campagnes sont organisées au profit des jeunes filles adolescentes en milieu scolaire. L’objectif est de les informer sur les causes, risques et conséquences des grossesses en milieu scolaire ainsi que les méthodes de prévention.
                2732 cas de grossesses ont été enregistrés au cours de l’année scolaire 2016-2017 et c’est pour remédier à cet état de choses qui joue sur l’éducation des filles que nous allons à leur rencontre.
                Ce projet est prévu pour s’étendre aux départements les plus touchés selon la classification du Ministère de l’Enseignement.
</br>
</br>
            </div>

        </div>
      </div>
</div>
</div>
</br>
</br>
<div id="page3">
<div class="container" style="background-color:#9ACD32;border:2px #9ACD32;border-radius:15px 50px;" >
<div class="container">
        <div class="intro-text">
            <div class="container ">
                <div class="container ">
                <strong> <em> Objectif zéro grossesse</em> </strong> :</br>

                « Objectif zéro grossesse  » est un projet qui consiste en des campagnes de sensibilisation. Ces campagnes sont organisées au profit des jeunes filles adolescentes en milieu scolaire. L’objectif est de les informer sur les causes, risques et conséquences des grossesses en milieu scolaire ainsi que les méthodes de prévention.
                2732 cas de grossesses ont été enregistrés au cours de l’année scolaire 2016-2017 et c’est pour remédier à cet état de choses qui joue sur l’éducation des filles que nous allons à leur rencontre.
                Ce projet est prévu pour s’étendre aux départements les plus touchés selon la classification du Ministère de l’Enseignement.
    
</br>
</br>
            </div>

        </div>
      </div>
</div>
</div>
</div>
    
</section>
@endsection