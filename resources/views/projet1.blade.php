@extends('layouts.add')
@section('content')
<br/>
<div class="page-header">
        <div class="container">
            <div class="row"><br/>
                <div class="col-12">
                
                    <h1>Objectif zéro grossesse</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="highlighted-cause">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7 order-2 order-lg-1">
                    <div class="entry-content mt-5" style="text-align:justify">
                        <p>« Objectif zéro grossesse » est un projet qui consiste en des campagnes de sensibilisation concernant la santé sexuelle et reproductive, un facteur qui joue souvent sur l’éducation des jeunes. Ces campagnes sont organisées au profit des jeunes en milieu scolaire de la 6e à la Terminale. L’objectif est de les informer sur les causes, risques et conséquences des maladies sexuellement transmissibles et des grossesses ainsi que les méthodes de prévention. 
                                Actuellement en phase 1 à Cotonou en partenariat avec l’Association Béninoise pour la Promotion de la Famille (ABPF), ce projet est prévu s’étendre également aux départements les plus touchés selon la classification du Ministère de l’Enseignement qui sont le Zou, le Borgou et l’Alibori. </p>
                    </div><!-- .entry-content -->

                   

                    <div class="entry-footer mt-6">
                        <a href="don" class="btn btn-primary" style="color:white">Faire un don</a>
                        <a href="projet1gal" class="btn btn-primary" style="color:white">Photos</a>
                    </div><!-- .entry-footer -->
                </div><!-- .col -->

                <div class="col-12 col-lg-5 order-1 order-lg-2">
                    <img src="img/gal/4.jpeg" alt="" style="width:312px;height:289px">
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .highlighted-cause -->
    @endsection