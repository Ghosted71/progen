@extends('layouts.add')
@section('content')

    <header class="masthead" >
        <div id="demo" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
                <li data-target="#demo" data-slide-to="3"></li>
                
            </ul>
        
            <!-- The slideshow -->
            <div class="carousel-inner">
                    <div class="carousel-item active">
                        <a href="propos">
                            <img src="../img/gal/ProGenBenin.png" alt="Chicago" style="width:100%;height:450px">
                            <div class="carousel-caption">
                                <div class="container">
                                    
                                </div>
                            </div>
                        </a>
                        </div>

                <div class="carousel-item">
                    <img src="../img/gal/1.JPG" alt="Chicago" style="width:400px;height:400px">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="intro-text">
                                <div class="intro-lead-in" style="color:black;"><strong>Objectif zero grossesses</strong></div>
                                <a class="btn btn-primary btn-xl text-uppercase" style="color:#F5F5DC;" href="don">Faire un don</a>
                                <a class="btn btn-primary btn-xl text-uppercase" style="color:#F5F5DC;" href="projet1">Savoir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="carousel-item">
                   <img src="../img/gal/3.jpeg" alt="Chicago" style="width:400px;height:400px">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="intro-text">
                                <div class="intro-lead-in" style="color:black;"><strong>Excellence Feminine</strong></div>
                                <a class="btn btn-primary btn-xl text-uppercase" style="color:#F5F5DC;" href="don">Faire un don</a>
                                <a class="btn btn-primary btn-xl text-uppercase" style="color:#F5F5DC;" href="projet3">Savoir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../img/gal/10.JPG" alt="New York" style="width:400px;height:400px">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="intro-text">
                                <div class="intro-lead-in" style="color:black;"><strong>Give A Smile</strong></div>
                                <a class="btn btn-primary btn-xl text-uppercase" style="color:#F5F5DC;" href="don">Faire un don</a>
                                <a class="btn btn-primary btn-xl text-uppercase" style="color:#F5F5DC;" href="projet2">Savoir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img src="../img/gal/10.JPG" alt="New York" style="width:400px;height:400px">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="intro-text">
                                <div class="intro-lead-in" style="color:black;"><strong>Enfance Epanouie</strong></div>
                                <a class="btn btn-primary btn-xl text-uppercase" style="color:#F5F5DC;" href="don">Faire un don</a>
                                <a class="btn btn-primary btn-xl text-uppercase" style="color:#F5F5DC;" href="projet2">Savoir Plus</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
      
     
    </header>
@endsection