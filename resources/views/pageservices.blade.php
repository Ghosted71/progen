@extends('layouts.add')
@section('content')
<section  id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Gallerie</h2>
            <h3 class="section-subheading text-muted">Nos albums temoignent de notre amour.</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6 portfolio-item">
            <a class="portfolio-link"  href="projet1gal">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/1.JPG" alt="Etude" style="width:400px;height:300px">
            </a>
            <div class="portfolio-caption">
              <h4>Objectif zéro grossesse </h4>
              <p class="text-muted"></p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 portfolio-item">
            <a class="portfolio-link"  href="projet2gal">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/10.JPG" alt="Etude" style="width:400px;height:300px">
            </a>
            <div class="portfolio-caption">
              <h4>Give a smile</h4>
              <p class="text-muted"></p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 portfolio-item">
            <a class="portfolio-link"  href="projet3gal">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/3.jpeg" alt="Etude" style="width:400px;height:300px">
            </a>
            <div class="portfolio-caption">
              <h4>Excellence Féminine </h4>
              <p class="text-muted"></p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 portfolio-item">
            <a class="portfolio-link"  href="projet4gal">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/25.JPG" alt="Etude" style="width:400px;height:300px">
            </a>
            <div class="portfolio-caption">
              <h4>Enfance épanouie</h4>
              <p class="text-muted"></p>
            </div>
          </div>
         
         
        </div>
      </div>
    </section>

@endsection