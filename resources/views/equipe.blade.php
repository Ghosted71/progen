@extends('layouts.add')
@section('content')
<br/>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Notre incroyable équipe</h2>
            <h3 class="section-subheading text-muted">ProGen est plus qu'une organisation. C'est une famille.</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="team-member">
                 <button type="button" style="background-color:steelblue" class="btn btn-primary rounded-circle" data-toggle="modal" data-target="#exampleModalLong1"> <img class="mx-auto rounded-circle" src="img/gal/13.jpeg" alt=""></button>
              <h4> Ghislaine AKAKPO</h4>
              <p class="text-muted">Présidente</p>
              <ul class="list-inline social-buttons">
               
               
             
                <li class="list-inline-item">
                  <a href="mailto:ghislaine.akakpo@progenbenin.org?Subject=Salut%ProGen" target="_top">
                    <i class="fa fa-envelope"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="team-member">
            <a tabindex="0" class="" role="button" data-toggle="popover" data-trigger="hover" title="cliquer pour plus d'information" data-content="Cliquer pour plus d'infos"> <button type="button" style="background-color:steelblue" class="btn btn-primary rounded-circle" data-toggle="modal" data-target="#exampleModalLong2" ><img class="mx-auto rounded-circle" src="img/gal/12.jpeg" alt=""></button></a>
              <h4>Aubierge MIKPONHOUE</h4>
              <p class="text-muted">Secrétaire Générale</p>
              <ul class="list-inline social-buttons">
               
               
                <li class="list-inline-item">
                  <a href="mailto:aubierge.mikponhoue@progenbenin.org?Subject=Salut%ProGen" target="_top">
                    <i class="fa fa-envelope"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="team-member">
             <button type="button" style="background-color:steelblue" class="btn btn-primary rounded-circle" data-toggle="modal" data-target="#"><img class="mx-auto rounded-circle" src="img/team/lucien.JPG" alt="Unknown"></button>
              <h4>Lucien KPLOCA</h4>
              <p class="text-muted">Trésorier</p>
              <ul class="list-inline social-buttons">
                
               
              
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <p class="large text-muted"></p>
          </div>
        </div>
        
      </div>

<!--
      <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h4 class="section-heading text-left">Volontaires</h4>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4">
              <div class="team-member">
                
                   <button type="button" style="background-color:steelblue" class="btn btn-primary rounded-circle"> <img class="mx-auto rounded-circle" src="img/ab.jpg" alt=""></button>
                
                
              </div>
            </div>
           
            
          </div>
          <div class="row">
            <div class="col-lg-8 mx-auto text-center">
              <p class="large text-muted"></p>
          
            </div>
          </div>
      </div>
    -->
    <!--
      <div class="container">
          <div class="row">
            <div class="col-lg-12 text-left">
              <h4 class="section-heading text-left">Nos Partenaires</h4>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4">
              <div class="team-member">
                 <a tabindex="0" class="" role="button" data-toggle="popover" data-trigger="hover" title="cliquer pour plus d'information" data-content="Cliquer pour plus d'infos">
                   <button type="button" style="background-color:steelblue" class="btn btn-primary rounded-circle" data-toggle="modal" data-target="#partner"> <img class="mx-auto rounded-circle" src="img/gal/ABPF.png" alt="" style="width:175px;height:190px"></button></a>
                <h4>A.B.P.F</h4>

                <ul class="list-inline social-buttons">
                  <li class="list-inline-item">
                    <a href="#">
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#">
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                  <li class="list-inline-item">
                    <a href="#">
                      <i class="fa fa-envelope"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
      </div>
    -->
      <div class="col-lg-12 text-center">
          <a type="button" class="btn btn-primary btn-xl text-uppercase" style="color:white" href="contact">Rejoindre la famille</a>
        </div>
          
        </div>
        


 <div class="modal fade" id="exampleModalLong1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fermé">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <strong> <em style="color: steelblue">Président et fondatrice de ProGen Bénin, chargée de la coordination et la supervision des activités.</em> </strong> <br/>

<p style="text-align: justify">
    Née à Parakou, Bénin dans une grande famille polygame et pauvre, elle s’est prise très tôt en charge en faisant de petits travaux pour subvenir à ses besoins jusqu'à l'obtention de son baccalauréat série G2.<br/>
    Seule intellectuelle de sa famille, les difficultés de la vie ne l'on pas empêché de continuer ses études universitaires jusqu'à l’obtention de sa Maitrise en Droit des Affaires à Université de Parakou et d’une Attestation Certifiée en Leadership Civic du Centre Africain d’Etudes Supérieures en Gestion au Dakar, Sénégal.<br/>
    
    Dotée d’une vision d’un monde dont les cadres éducatifs sont favorables aux études et d’une envie d’aider les enfants qui viennent de la même situation qu’elle, elle fit usage de son éducation et compétences et fonda ProGen Bénin.<br/>
    Passionnée des questions sociales et d'intégration, elle aime voyager, aller à la découverte d'autres cultures afin d'enrichir ses connaissances et  écouter de la musique.<br/> </p>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermé</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModalLong2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <strong> <em style="color:steelblue">Secrétaire Générale, assiste dans l’exécution de toutes les taches inhérentes au fonctionnement.</em> </strong> <br/>


        <p style="text-align: justify">

            Née à Cotonou au Bénin, sa passion pour la lecture et la justice l’amena à obtenir son  baccalauréat littéraire, sa Maitrise en Droit à l’Université d’Abomey-Calavi ainsi que son Master en Gestion des  Ressources Humaines et Communication à l'Ecole Nationale d'Economie Appliquée et de Management (ENEAM).<br/> 
            Soucieuse d'apporter son grain pour une vie meilleure autour d'elle, elle a plusieurs fois  accompagné des amis dans les dons aux orphelins et aux enfants en situations difficiles et pris part aux activités de WANEP Bénin en tant que militante volontaire. <br/>
            Déterminée de contribuer directement,  elle s’engagea à ProGen Bénin dans le but de faire de l'éducation un droit pour tous les enfants. Elle reste convaincue d'avoir fait le bon choix parce que le constat sur le terrain montre que beaucoup reste à faire dans ce domaine.<br/>	
            </p>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermé</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModalLong3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <strong> <em style="color:steelblue">Trésorier, chargé de la gestion financière</em> </strong><br/>
  
          <p style="text-align: justify">
  
              </p>
  
  
  
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermé</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="partner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <strong> <em>Association Béninoise de Promotion de la Famille (ABPF)</em></strong><br/>
    <p style="text-align: justify">
        L’ABPF a pour but principal de contribuer à l’amélioration qualitative de la santé sexuelle et de la Reproduction des populations vivant au Bénin et de contribuer à faire respecter les droits des femmes, des hommes, des jeunes et autres personnes vulnérables d’effectuer des choix libres et informés en ce qui concerne leur santé sur le plan de la sexualité et de la reproduction.
    </p>
    
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    @endsection
