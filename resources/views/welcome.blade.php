
@extends('layouts.add')
@section('content')
<div id="demo" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
    </ul>

    <!-- The slideshow -->
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="../img/banner1.jpg" alt="Los Angeles" >
        </div>
        <div class="carousel-item">
            <img src="../img/banner2.jpg" alt="Chicago" width="1100" height="500">
            <div class="carousel-caption">
                <div class="container">
                    <div class="intro-text">
                        <div class="intro-lead-in">(Slogan)</div>
                        <a class="btn btn-primary btn-xl text-uppercase" style="color:#F5F5DC;;" href="#services">Faire un don!</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img src="../img/banner3.jpg" alt="New York" width="1100" height="500">
            <div class="carousel-caption">
                <div class="container">
                    <div class="intro-text">
                        <div class="intro-lead-in">(Slogan)</div>
                        <a class="btn btn-primary btn-xl text-uppercase" style="color:#F5F5DC;;" href="#services">Faire un don!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>

@endsection
