@extends('layouts.add')
@section('content')
<br/>
<div class="page-header">
        <div class="container">
            <div class="row"><br/>
                <div class="col-12">
                
                    <h1>Excellence Féminine</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="highlighted-cause">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7 order-2 order-lg-1">
                    <div class="entry-content mt-5" style="text-align:justify">
                        <p>« Excellence Féminine » est un projet d’octroi de bourses au profit des élèves de sexe féminin de toutes classes ayant démontrés une excellence dans leurs études au cours d’une année scolaire. Intervenant actuellement dans les écoles primaires du Borgou, Alibori et Atacora, ce projet consiste à sélectionner les élèves les plus méritantes de chaque classe à qui on offre une bourse complète pour l’année scolaire suivante, couvrant les fournitures et frais de scolarités.<br/>
                                Cette approche vise à augmenter la motivation des filles à exceller dans leurs études en faisant une promotion de l’excellence à chaque fin d’année, tout en leur offrant l’opportunité de continuer leurs études. </p>
                    </div><!-- .entry-content -->

                    

                    <div class="entry-footer mt-6">
                        <a href="don" class="btn btn-primary" style="color:white">Faire un don</a>
                        <a href="projet3gal" class="btn btn-primary" style="color:white">Photos</a>
                    </div><!-- .entry-footer -->
                </div><!-- .col -->

                <div class="col-12 col-lg-5 order-1 order-lg-2">
                    <img src="img/gal/3.jpeg" alt="" style="width:312px;height:289px">
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .highlighted-cause -->



   
    @endsection