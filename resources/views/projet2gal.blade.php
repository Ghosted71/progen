@extends('layouts.add')
@section('content')
<section  id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase" style="color:steelblue">Give A Smile</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-4 portfolio-item">
            <a class="portfolio-link"  href="#">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/5.JPG" alt="Etude" style="width:400px;height:300px">
            </a>
           
          </div>
          <div class="col-md-4 col-sm-4 portfolio-item">
            <a class="portfolio-link"  href="#">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/10.JPG" alt="Etude" style="width:400px;height:300px">
            </a>
           
          </div>
          <div class="col-md-4 col-sm-4 portfolio-item">
            <a class="portfolio-link"  href="#">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/gal/15.JPG" alt="Etude" style="width:400px;height:300px">
            </a>
            
          </div>
          
         
         
        </div>
        <div style="text-align:center">
            <a href="don" class="btn btn-primary" style="color:white">Faire un don</a>
            <a href="projet2" class="btn btn-primary" style="color:white">Savoir Plus</a>
        </div><!-- .entry-footer -->
      </div>
     
    </section>

@endsection