@extends('layouts.add')
@section('content')
<br/>
<div class="page-header">
        <div class="container">
            <div class="row"><br/>
                <div class="col-12">
                
                    <h1>Give A Smile</h1>
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .page-header -->

    <div class="highlighted-cause">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7 order-2 order-lg-1">
                    <div class="entry-content mt-5" style="text-align:justify">
                        <p>« Give a smile » (Donner le Sourie) est un projet de parrainage d’enfants qui consiste à s’engager vis  à vis d’un enfant en prenant en charge ses besoins scolaires sur une période donnée, dont les fournitures, les frais, des maîtres d’études, etc.). Les contributions et engagements bénéficieront pour la plus part des enfants en situations difficiles (orphelins, placés ou en manque de soutiens…).<br/> 
                                Intervenant actuellement dans les écoles primaires du Borgou, Alibori et Atacora, ce projet vise à donner le sourire à ces enfants qui malgré leurs volonté n’auraient jamais eu la chance d’être scolariser si une bonne volonté n’intervenait pas.</p>
                    </div><!-- .entry-content -->

                   

                    <div class="entry-footer mt-6">
                        <a href="don" class="btn btn-primary" style="color:white">Faire un don</a>
                        <a href="projet2gal" class="btn btn-primary" style="color:white">Photos</a>
                    </div><!-- .entry-footer -->
                </div><!-- .col -->

                <div class="col-12 col-lg-5 order-1 order-lg-2">
                    <img src="img/gal/10.JPG" alt="" style="width:312px;height:289px">
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .highlighted-cause -->



   
    @endsection